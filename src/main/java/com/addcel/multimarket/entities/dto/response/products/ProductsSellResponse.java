package com.addcel.multimarket.entities.dto.response.products;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductsSellResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	private Integer preBalance;
	private Integer posBalance;
	private Integer debt;
	private DataResponse data;
}
