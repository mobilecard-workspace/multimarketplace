package com.addcel.multimarket.entities.dto.response.services;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillDataResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer amount;
	private Integer productId;
	private String productName;
	private String reference;
	private String hashEchoData;
	private String hash;
	private Boolean amountEditable;
}
