package com.addcel.multimarket.entities.dto.response.services;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillSearchListResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SerializedName("productId")
	private Integer productId;
	@SerializedName("name")
	private String name;
	@SerializedName("image")
	private String image;
	@SerializedName("key")
	private String key;
	@SerializedName("category")
	private String category;
}
