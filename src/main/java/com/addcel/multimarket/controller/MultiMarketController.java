package com.addcel.multimarket.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.multimarket.entities.dto.request.products.ProductsNoAmountRequest;
import com.addcel.multimarket.entities.dto.request.products.ProductsSellRequest;
import com.addcel.multimarket.entities.dto.request.services.ProductQuery;
import com.addcel.multimarket.entities.dto.request.services.ServicePayRequest;
import com.addcel.multimarket.entities.dto.response.products.ProductsSellResponse;
import com.addcel.multimarket.entities.dto.response.services.BillDataResponse;
import com.addcel.multimarket.entities.dto.response.services.BillSearchListResponse;
import com.addcel.multimarket.errors.ResponseException;
import com.addcel.multimarket.service.MultiMarketClient;
import com.addcel.multimarket.utils.MultiMarketUtils;

@RestController
public class MultiMarketController {
	
	@Autowired
	private MultiMarketClient client;
	
	private static final Logger LOG = LoggerFactory.getLogger(MultiMarketController.class);
	
	@ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseException> handleException(Exception e) {
		int code = -1;
		String message = e.getMessage();
		LOG.error("Error al consumir servicio: "+e.getLocalizedMessage());
        e.printStackTrace();
        return new ResponseEntity<ResponseException>(new ResponseException(code, message), HttpStatus.OK);
    }

	@PostMapping(value = MultiMarketUtils.CTRL_COMPRA_AMOUNT)
	ResponseEntity<ProductsSellResponse> comprarProducto(@RequestBody ProductsSellRequest request) throws Exception {
		return new ResponseEntity<ProductsSellResponse>(client.compraProductos(request), HttpStatus.OK);
	}
	
	@PostMapping(value = MultiMarketUtils.CTRL_COMPRA)
	ResponseEntity<ProductsSellResponse> comprarProductoNoAmount(@RequestBody ProductsNoAmountRequest request) throws Exception {
		return new ResponseEntity<ProductsSellResponse>(client.compraProductos(request), HttpStatus.OK);
	}
	
	@PostMapping(value = MultiMarketUtils.CTRL_CONSULTA_SERVICIO)
	ResponseEntity<List<BillSearchListResponse>> consultarServicio(@RequestBody ProductQuery query) throws Exception{
		return new ResponseEntity<List<BillSearchListResponse>>(client.consultaServicio(query), HttpStatus.OK);
	}
	
	@PostMapping(value = MultiMarketUtils.CTRL_GENERATE_HASH)
	ResponseEntity<BillDataResponse> consultarFactura(@RequestBody ProductQuery query) throws Exception{
		return new ResponseEntity<BillDataResponse>(client.consultaFactura(query), HttpStatus.OK);
	}
	
	@PostMapping(value = MultiMarketUtils.CTRL_PAGO_SERVICIO)
	ResponseEntity<ProductsSellResponse> pagarServicio(@RequestBody ServicePayRequest request) throws Exception{
		return new ResponseEntity<ProductsSellResponse>(client.compraProductos(request), HttpStatus.OK);
	}
 }
