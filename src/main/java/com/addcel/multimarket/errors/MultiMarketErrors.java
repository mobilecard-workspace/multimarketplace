package com.addcel.multimarket.errors;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MultiMarketErrors implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String title;
	private List<Errors> errors;
}
