package com.addcel.multimarket.errors;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Errors implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String location;
	private String param;
	private String msg;
}
