package com.addcel.multimarket;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.addcel.multimarket.entities.dto.ConfigMultiMarket;
import com.addcel.multimarket.repositories.MultiMarketRepository;

@SpringBootApplication
public class MultiMarketPlaceApplication extends SpringBootServletInitializer{
	
	@Autowired
	private MultiMarketRepository repo;
	
	private static final Logger LOG = LoggerFactory.getLogger(MultiMarketPlaceApplication.class);
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
		return app.sources(MultiMarketPlaceApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MultiMarketPlaceApplication.class, args);
	}
	
	@Bean
	@PostConstruct
	public ConfigMultiMarket getToken() throws Exception {
		ConfigMultiMarket token = null;
		LOG.info("Obteniendo el token para multimarket Place");
		try {
			token = repo.findById(1L).orElse(null);
			if (token != null) {
				LOG.info("Token recuperado con exito");
			} else {
				LOG.error("No existe el token");
			}
		} catch (Exception e) {
			LOG.error("Error al recuperarlas credenciales de SOAT "+e.getMessage());
			throw new Exception("Error al recuperar el token de MultiMarket");
		}
		return token;
	}
}
