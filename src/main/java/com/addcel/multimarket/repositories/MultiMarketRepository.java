package com.addcel.multimarket.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.addcel.multimarket.entities.dto.ConfigMultiMarket;

public interface MultiMarketRepository extends JpaRepository<ConfigMultiMarket, Long> {

}
