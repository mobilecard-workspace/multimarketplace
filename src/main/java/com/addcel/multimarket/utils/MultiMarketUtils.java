package com.addcel.multimarket.utils;

public class MultiMarketUtils {

	public static final String URL_MULTIMARKET_QA = "https://testing.refacil.co/api/v1/products/";
	public static final String URL_MULTIMARKET = "https://plataforma.refacil.co/api/v1/products/";
	public static final String CTRL_COMPRA_AMOUNT = "/api/v1/compraProductoAmount";
	public static final String CTRL_COMPRA = "/api/v1/compraProducto";
	public static final String CTRL_CONSULTA_SERVICIO = "/api/v1/consultaServicio";
	public static final String CTRL_GENERATE_HASH = "/api/v1/generateHash";
	public static final String CTRL_PAGO_SERVICIO = "api/v1/pagoServicio";
}
