package com.addcel.multimarket.service;

import java.util.List;

import com.addcel.multimarket.entities.dto.request.products.ProductsNoAmountRequest;
import com.addcel.multimarket.entities.dto.request.products.ProductsSellRequest;
import com.addcel.multimarket.entities.dto.request.services.ProductQuery;
import com.addcel.multimarket.entities.dto.request.services.ServicePayRequest;
import com.addcel.multimarket.entities.dto.response.products.ProductsSellResponse;
import com.addcel.multimarket.entities.dto.response.services.BillDataResponse;
import com.addcel.multimarket.entities.dto.response.services.BillSearchListResponse;

public interface MultiMarketClient {

	ProductsSellResponse compraProductos(ProductsSellRequest request) throws Exception;
	ProductsSellResponse compraProductos(ProductsNoAmountRequest request) throws Exception;
	List<BillSearchListResponse> consultaServicio(ProductQuery query) throws Exception;
	BillDataResponse consultaFactura(ProductQuery query) throws Exception;
	ProductsSellResponse compraProductos(ServicePayRequest request)throws Exception;
}
