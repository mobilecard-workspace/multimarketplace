package com.addcel.multimarket.service;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.addcel.multimarket.entities.dto.ConfigMultiMarket;
import com.addcel.multimarket.entities.dto.request.products.ProductsNoAmountRequest;
import com.addcel.multimarket.entities.dto.request.products.ProductsSellRequest;
import com.addcel.multimarket.entities.dto.request.services.ProductQuery;
import com.addcel.multimarket.entities.dto.request.services.ServicePayRequest;
import com.addcel.multimarket.entities.dto.response.products.ProductsSellResponse;
import com.addcel.multimarket.entities.dto.response.services.BillDataResponse;
import com.addcel.multimarket.entities.dto.response.services.BillSearchListResponse;
import com.addcel.multimarket.errors.MultiMarketErrors;
import com.addcel.multimarket.utils.MultiMarketUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class MultiMarketClientImpl implements MultiMarketClient {
	
	@Autowired
	private ConfigMultiMarket config;
	
	private static final Logger LOG = LoggerFactory.getLogger(MultiMarketClientImpl.class);
	private Gson GSON = new Gson();

	@Override
	public ProductsSellResponse compraProductos(ProductsSellRequest request) throws Exception {
		ProductsSellResponse response = null;
		LOG.info("Iniciando compra de producto: {}",request);
		try {
			ResponseEntity<String> result = sendRequest(request, "sell");
			
			LOG.info("Respuesta de MultiMarket: Estado "+result.getStatusCode()+", body "+result.getBody());
			
			if (result.getStatusCode().is2xxSuccessful()) {
				response = GSON.fromJson(result.getBody(), ProductsSellResponse.class);
			}else {
				MultiMarketErrors error = GSON.fromJson(result.getBody(), MultiMarketErrors.class);
				throw new Exception("Error "+error.getErrors().get(0).getMsg()+", en el parametro: "+error.getErrors().get(0).getParam());
			}
		} catch (Exception e) {
			LOG.info("Error al consumir MultiMarket: "+e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	@Override
	public ProductsSellResponse compraProductos(ProductsNoAmountRequest request) throws Exception {
		ProductsSellResponse response = null;
		LOG.info("Iniciando compra de producto sin amount: {}",request);
		try {
			ResponseEntity<String> result = sendRequest(request, "sell");
			
			LOG.info("Respuesta de MultiMarket: Estado "+result.getStatusCode()+", body "+result.getBody());
			
			if (result.getStatusCode().is2xxSuccessful()) {
				response = GSON.fromJson(result.getBody(), ProductsSellResponse.class);
			}else {
				MultiMarketErrors error = GSON.fromJson(result.getBody(), MultiMarketErrors.class);
				throw new Exception("Error "+error.getErrors().get(0).getMsg()+", en el parametro: "+error.getErrors().get(0).getParam());
			}
		} catch (Exception e) {
			LOG.info("Error al consumir MultiMarket: "+e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}

	@Override
	public List<BillSearchListResponse> consultaServicio(ProductQuery query) throws Exception {
		List<BillSearchListResponse> response = null;
		LOG.info("Consultando el servicio: {}",query);
		try {
			ResponseEntity<String> result = sendRequest(query, "query");
			
			LOG.info("Respuesta de MultiMarket: Estado "+result.getStatusCode()+", body "+result.getBody());
			
			if (result.getStatusCode().is2xxSuccessful()) {
				response = GSON.fromJson(result.getBody(), new TypeToken<ArrayList<BillSearchListResponse>>() {}.getType());
			}else {
				MultiMarketErrors error = GSON.fromJson(result.getBody(), MultiMarketErrors.class);
				throw new Exception("Error "+error.getErrors().get(0).getMsg()+", en el parametro: "+error.getErrors().get(0).getParam());
			}
		} catch (Exception e) {
			LOG.info("Error al consumir MultiMarket: "+e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}

	@Override
	public BillDataResponse consultaFactura(ProductQuery query) throws Exception {
		BillDataResponse response = null;
		LOG.info("Generando HASH: {}", query);
		try {
			ResponseEntity<String> result = sendRequest(query, "query");
			
			LOG.info("Respuesta de MultiMarket: Estado "+result.getStatusCode()+", body "+result.getBody());
			
			if (result.getStatusCode().is2xxSuccessful()) {
				response = GSON.fromJson(result.getBody(), BillDataResponse.class);
			}else {
				MultiMarketErrors error = GSON.fromJson(result.getBody(), MultiMarketErrors.class);
				throw new Exception("Error "+error.getErrors().get(0).getMsg()+", en el parametro: "+error.getErrors().get(0).getParam());
			}
		} catch (Exception e) {
			LOG.info("Error al consumir MultiMarket: "+e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}

	@Override
	public ProductsSellResponse compraProductos(ServicePayRequest request) throws Exception {
		ProductsSellResponse response = null;
		LOG.info("Iniciando compra de producto: {}",request);
		try {
			ResponseEntity<String> result = sendRequest(request, "sell");
			
			LOG.info("Respuesta de MultiMarket: Estado "+result.getStatusCode()+", body "+result.getBody());
			
			if (result.getStatusCode().is2xxSuccessful()) {
				response = GSON.fromJson(result.getBody(), ProductsSellResponse.class);
			}else {
				MultiMarketErrors error = GSON.fromJson(result.getBody(), MultiMarketErrors.class);
				throw new Exception("Error "+error.getErrors().get(0).getMsg()+", en el parametro: "+error.getErrors().get(0).getParam());
			}
		} catch (Exception e) {
			LOG.info("Error al consumir MultiMarket: "+e.getMessage());
			throw new Exception(e.getMessage());
		}
		return response;
	}
	
	private ResponseEntity<String> sendRequest(Object request, String serviceUri) throws Exception{
		ResponseEntity<String> result = null;
		try {
			LOG.info("Enviando la peticion {}",request);
			String url = MultiMarketUtils.URL_MULTIMARKET+serviceUri;
			LOG.info("Url: "+url);
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
		    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
		            .loadTrustMaterial(null, acceptingTrustStrategy).build();
		    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
		    CloseableHttpClient httpClient = HttpClients.custom()
		            .setSSLSocketFactory(csf).build();
		    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		    requestFactory.setHttpClient(httpClient);
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Bearer "+config.getToken());
			
			String jsonRequest = GSON.toJson(request);
			
			LOG.info("JSON Request: {}", jsonRequest);
			
			HttpEntity<String> entity = new HttpEntity<String>(jsonRequest,headers);
			result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			
		} catch (Exception e) {
			LOG.info("Error al procesar la aplicacion: "+e.getMessage());
			throw new Exception(e.getMessage());
		}
		return result;
	}

}
